/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.nguyenkim.shopping.shoppingcordovaplugin;


import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.apache.cordova.engine.SystemWebChromeClient;
import org.apache.cordova.engine.SystemWebView;
import org.apache.cordova.engine.SystemWebViewEngine;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nguyenkim.shopping.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class shoppingcordovaplugin extends CordovaPlugin {

    private Button retryButton;
    private ViewGroup overlay;
    private ProgressBar progressBar;
    private TextView messageText;

    private String TAG;

    /**
     * Constructor.
     */
    public shoppingcordovaplugin() {

        LOG.d(TAG, "shoppingcordovaplugin is being created");
    }


    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        TAG = this.getServiceName();
        LOG.d(TAG, "shoppingcordovaplugin is initializing...");
        // Add Extend Views
        this.addExtendView();
    }

    @SuppressLint("InflateParams")
    private void addExtendView() {
        LOG.d(TAG, "shoppingcordovaplugin is adding action bar()");

        // Set background color to white
        if (this.webView.getView().getParent() instanceof View) {
            ((View) this.webView.getView().getParent()).setBackgroundColor(Color.WHITE);
        }

        LayoutInflater inflater = LayoutInflater.from(this.cordova.getActivity());
        View layout = inflater.inflate(R.layout.top_action_bar, null); // Get layout for app
        this.cordova.getActivity().addContentView(layout, new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1.0F));

        // Find views from layout
        progressBar = (ProgressBar) this.cordova.getActivity().findViewById(R.id.progressBar);
        retryButton = (Button) this.cordova.getActivity().findViewById(R.id.retryButton);
        messageText = (TextView) this.cordova.getActivity().findViewById(R.id.messageText);
        overlay = (ViewGroup) this.cordova.getActivity().findViewById(R.id.overlay);

        // Bind events to views
        retryButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isReloading = true;
                webView.loadUrl(failingUrl); // Reload error URL
            }
        });

        // Progress bar
        if (this.webView.getEngine() instanceof SystemWebViewEngine
                && this.webView.getEngine().getView().getClass().equals(SystemWebView.class)) {
            SystemWebViewEngine systemEngin = (SystemWebViewEngine) this.webView.getEngine();
            SystemWebView systemWebview = (SystemWebView) systemEngin.getView();
            systemWebview.setWebChromeClient(new SystemWebChromeClient(systemEngin) {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    super.onProgressChanged(view, newProgress);
                    progressBar.setProgress(newProgress);
                    if (newProgress > 40) {
                        showWebView();
                    }
                }
            });
        }
    }

    protected void showWebView() {
        long difference = System.currentTimeMillis() - startTime;
        if (isLoading && webviewIsInvisible && difference > 1000) {
            AlphaAnimation alphaAnimationForWebview = new AlphaAnimation(0, 1);
            alphaAnimationForWebview.setDuration(1000L);
            alphaAnimationForWebview.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    overlay.setVisibility(View.INVISIBLE); // Hide overlay
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    webView.getView().setVisibility(View.VISIBLE); // Show webview
                    progressBar.setVisibility(View.GONE);
                }
            });
            webView.getView().startAnimation(alphaAnimationForWebview);
            webviewIsInvisible = false;
            canMakeNewRequest = true; // Mark that user can make new request now
            isGoingBack = true; // Mark next request is go-back one. This will be reassign to false in function onOverrideUrlLoading if the next request is a user-click-link one
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException {
        switch (requestCode) {
            case 1:
                break;
            default:
                break;
        }
    }

    //--------------------------------------------------------------------------
    // BEGIN: Javascript Interface
    //--------------------------------------------------------------------------

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("showView".equals(action)) {
            try {
                String viewName = args.getString(0);
                this.showView(viewName);
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "Succeed"));
            } catch (Exception ex) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, ex.getMessage()));
            }
        } else if ("hideView".equals(action)) {
            try {
                String viewName = args.getString(0);
                this.hideView(viewName);
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "Succeed"));
            } catch (Exception ex) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, ex.getMessage()));
            }
        }
        return true;
    }

    /**
     * @param viewName Name of view to show.
     * @return true if view is able to show.
     */
    private boolean showView(String viewName) {

        return true;
    }

    /**
     * @param viewName Name of view to hide.
     * @return true if view is able to hide.
     */
    private boolean hideView(String viewName) {
        Activity activity = this.cordova.getActivity();

        return true;
    }

    //--------------------------------------------------------------------------
    // END: Javascript Interface
    //--------------------------------------------------------------------------


    @Override
    public Object onMessage(String id, Object data) {
        // Call method of super
        Object result = super.onMessage(id, data);
        // Process messages
        if ("onPageStarted".equals(id)) {
            this.onPageStarted((String) data);
        } else if ("onPageFinished".equals(id)) {
            this.onPageFinished((String) data);
        } else if ("onReceivedError".equals(id)) {
            JSONObject jData = (JSONObject) data;
            int errorCode = Integer.MIN_VALUE;
            String errorDescription = "Unknow error";
            String errorUrl = "Unknow URL";
            // Get error code from data
            try {
                errorCode = jData.getInt("errorCode");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Get description code from data
            try {
                errorDescription = jData.getString("description");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Get error URL from data
            try {
                errorUrl = jData.getString("url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (errorCode != -6 || !webviewIsInvisible) {
                this.onReceivedError(errorCode, errorDescription, errorUrl);
            }
            // Report to the host application that the error was handled
            return true;
        }
        // Return result of method of super
        return result;
    }

    private boolean isLoading = false; // Flag, set if webView is loading
    private boolean isFirstLoading = true;
    private boolean isGoingBack = false;
    private boolean isReloading = false;
    private boolean canMakeNewRequest = true;
    private boolean webviewIsInvisible = false;

    @Override
    public boolean onOverrideUrlLoading(String url) {
        org.apache.cordova.LOG.d(TAG, "shouldOverrideUrlLoading(): " + url);
        isGoingBack = false; // When back button is pressed, this function is not called. So we use this function to mark the request is not go-back request
        return super.onOverrideUrlLoading(url);
    }

    private Handler handler = new Handler();
    private String failingUrl;
    private long startTime;

    protected void onPageStarted(String url) {
        if (!isLoading || canMakeNewRequest) {
            isLoading = true;
            canMakeNewRequest = false;
            startTime = System.currentTimeMillis();
            if (isGoingBack) {
                // Make a go-back animation
                TranslateAnimation translateAniForWebview = new TranslateAnimation(0, webView.getView().getWidth(), 0, 0);
                translateAniForWebview.setInterpolator(new DecelerateInterpolator(1.75f));
                translateAniForWebview.setDuration(500L);
                translateAniForWebview.setAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        overlay.setVisibility(View.VISIBLE); // Show overlay
                        webView.getView().setVisibility(View.INVISIBLE); // Hide webview
                    }
                });
                webView.getView().startAnimation(translateAniForWebview);
            } else if (isFirstLoading || isReloading) {
                webView.getView().setVisibility(View.INVISIBLE); // Hide webview
                overlay.setVisibility(View.VISIBLE); // Show overlay
            } else {
                // Make a go-forward animation
                TranslateAnimation translateAniForOverlay = new TranslateAnimation(webView.getView().getWidth(), 0, 0, 0);
                translateAniForOverlay.setInterpolator(new DecelerateInterpolator(1.75f));
                translateAniForOverlay.setDuration(500L);
                translateAniForOverlay.setAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        overlay.setVisibility(View.VISIBLE); // Show overlay
                        webView.getView().setVisibility(View.INVISIBLE); // Hide webview
                    }
                });
                overlay.startAnimation(translateAniForOverlay);
            }
            webviewIsInvisible = true;
            progressBar.setVisibility(View.VISIBLE); // Show progress bar
            retryButton.setVisibility(View.INVISIBLE); // Hide retry button
            messageText.setText(R.string.loading);
            messageText.setVisibility(View.VISIBLE); // Hide message label
        }
    }

    private boolean isReceivedError = false;

    protected void onReceivedError(int errorCode, String description, String failingUrl) {
        if (isLoading) {
            this.failingUrl = failingUrl;
            this.isReceivedError = true; // Mark that we received an error
            if (errorCode == -6) { // If error is timeout error, stop load immediately
                this.onPageFinished(failingUrl);
                Activity activity = this.cordova.getActivity();
//                if (activity instanceof TNDActivity) {
//                    ((TNDActivity) activity).webViewEngin.stopLoading();
//                }
            }
        }
    }

    protected void onPageFinished(String url) {
        if (isLoading) {
            if (isReceivedError) {
                // Delay 1 second and then show error message
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        overlay.setVisibility(View.VISIBLE); // Show overlay
                        messageText.setText(R.string.no_netwwork); // Assign error description for message label
                        messageText.setVisibility(View.VISIBLE); // Show message label
                        retryButton.setVisibility(View.VISIBLE); // Show retry button
                        progressBar.setVisibility(View.GONE); // Hide progress bar
                        webView.getView().setVisibility(View.INVISIBLE);
                    }
                }, 1000);
            } else {
                overlay.setVisibility(View.INVISIBLE); // Hide overlay
                messageText.setText(null); // Clear message label
                messageText.setVisibility(View.INVISIBLE); // Hide message label
                retryButton.setVisibility(View.INVISIBLE); // Hide retry button
                progressBar.setVisibility(View.GONE); // Hide progress bar
                webView.getView().setVisibility(View.VISIBLE);
                webviewIsInvisible = false;
                isFirstLoading = false;
            }
            isLoading = false;
            isGoingBack = true; // Mark next request is go-back one. This will be reassign to false in function onOverrideUrlLoading if the next request is a user-click-link one 
            isReloading = false;
            isReceivedError = false;
        }
    }
}

/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#import <Cordova/CDV.h>
#import "TNDshoppingcordovaplugin.h"

@interface TNDshoppingcordovaplugin () {
    NSString *TAG;
    UINavigationBar *navigationBar;
    UIButton *backButton;
    UIButton *menuButton;
    UIButton *titleButton;
    UIButton *cartButton;
    UIButton *shareButton;
    UIButton *retryButton;
    UILabel *txtStatus;
    UIImageView *bgTetView;

    UIProgressView* progressBar;
    UIActivityIndicatorView *progressRing;
    NSTimer *timer;

    id<UIWebViewDelegate> originWebViewDelegate;
}
- (void)_showView:(NSString*)viewName;
- (void)_hideView:(NSString*)viewName;
- (void)_setText:(NSString*)text forView:(NSString*)viewName;
- (void)_registerViewClickedEventListener:(NSString*)viewName listener:(NSString*)jsListerner;
- (void)_unregisterViewClickedEventListener:(NSString*)viewName listener:(NSString*)jsListener;
- (void)_clearCache;
- (void)_clearHistory;
- (BOOL)_canGoBack;
@end

@implementation TNDshoppingcordovaplugin

+ (NSString*)cordovaVersion
{
    return CDV_VERSION;
}

- (void)pluginInitialize{
    TAG = @"TNDshoppingcordovaplugin";
    NSLog(@"%@ pluginInitialize", TAG);
    [super pluginInitialize];
    [self addNavigationBar]; // Add extend views
}

- (void)handleOpenURL:(NSNotification*)notification{
    NSLog(@"%@ handleOpenURL", TAG);
    [super handleOpenURL:notification];
}
- (void)onAppTerminate{
    NSLog(@"%@ onAppTerminate", TAG);
    [super onAppTerminate];
}
- (void)onMemoryWarning{
    NSLog(@"%@ onMemoryWarning", TAG);
    [super onMemoryWarning];
}
- (void)onReset{
    NSLog(@"%@ onReset", TAG);
    [super onReset];
}
- (void)dispose{
    NSLog(@"%@ dispose", TAG);
    [super dispose];
}

- (void)addNavigationBar{
    NSLog(@"%@ addNavigationBar", TAG);

    // Turn off translate autoesizing into constraint
    [self.viewController.view setTranslatesAutoresizingMaskIntoConstraints:NO];

    // Create navigation bar
    navigationBar = [[UINavigationBar alloc] init];
    [navigationBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    navigationBar.barTintColor = [UIColor colorWithRed:(220.0/255.0) green:(0.0/255.0) blue:(33.0/255.0) alpha:1];
    navigationBar.translucent = NO;

    // Add navigation bar to root view
    [self.viewController.view addSubview:navigationBar];
    // Add constrain to navigation bar
    NSLayoutConstraint *navBarLeftConstraint = [NSLayoutConstraint constraintWithItem:navigationBar
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.self.viewController.view
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:1.0 constant:0.0];
    NSLayoutConstraint *navBarRightConstraint = [NSLayoutConstraint constraintWithItem:navigationBar
                                                                             attribute:NSLayoutAttributeRight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.self.viewController.view
                                                                             attribute:NSLayoutAttributeRight
                                                                            multiplier:1.0 constant:0.0];
    NSLayoutConstraint *navBarTopConstraint = [NSLayoutConstraint constraintWithItem:navigationBar
                                                                           attribute:NSLayoutAttributeTop
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.self.viewController.view
                                                                           attribute:NSLayoutAttributeTop
                                                                          multiplier:1.0 constant:0.0];
    NSLayoutConstraint *navBarBottomConstraint = [NSLayoutConstraint constraintWithItem:navigationBar
                                                                              attribute:NSLayoutAttributeBottom
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.self.viewController.view
                                                                              attribute:NSLayoutAttributeTop
                                                                             multiplier:1.0 constant:64.0];
    [self.viewController.view addConstraint:navBarLeftConstraint];
    [self.viewController.view addConstraint:navBarRightConstraint];
    [self.viewController.view addConstraint:navBarTopConstraint];
    [self.viewController.view addConstraint:navBarBottomConstraint];

    // Create Back Button
    backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backButton setImage:[UIImage imageNamed:@"ic_arrow_back"] forState:UIControlStateNormal];
    backButton.tintColor = [UIColor whiteColor];
    //[backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    // Create Home Button
    menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [menuButton setImage:[UIImage imageNamed:@"ic_menu"] forState:UIControlStateNormal];
    menuButton.tintColor = [UIColor whiteColor];
    //[menuButton setTitle:@"Menu" forState:UIControlStateNormal];
    [menuButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    // Create Title Button
    titleButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    titleButton.tintColor = [UIColor whiteColor];
    [titleButton setTitleColor: [UIColor blackColor] forState:UIControlStateNormal];
    titleButton.titleLabel.font = [UIFont systemFontOfSize:20.0];
    [titleButton setImage:[UIImage imageNamed:@"ic_logo_nk"] forState:UIControlStateNormal];
    [titleButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    // Create Shopping Cart Button
    cartButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cartButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cartButton setImage:[UIImage imageNamed:@"ic_shopping_cart"] forState:UIControlStateNormal];
    cartButton.tintColor = [UIColor whiteColor];
    [cartButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    // Create Search Button
    shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [shareButton setImage:[UIImage imageNamed:@"ic_share"] forState:UIControlStateNormal];
    shareButton.tintColor = [UIColor whiteColor];
    [shareButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    // Set padding for Button
    backButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    menuButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    cartButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    shareButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);

    // BEGIN: Hinh background tet
    UIImage *bg_tet = [UIImage imageNamed:@"bg_tet"];
    bgTetView = [[UIImageView alloc] initWithImage:bg_tet];
    [bgTetView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [navigationBar addSubview:bgTetView];
    NSLayoutConstraint *bgTetViewCenterXConstraint = [NSLayoutConstraint constraintWithItem:bgTetView
                                                                                 attribute:NSLayoutAttributeCenterX
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:navigationBar
                                                                                 attribute:NSLayoutAttributeCenterX
                                                                                multiplier:1.0 constant:0];
    NSLayoutConstraint *bgTetViewCenterYConstraint = [NSLayoutConstraint constraintWithItem:bgTetView
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:navigationBar
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                multiplier:1.0 constant:6];
    [navigationBar addConstraint:bgTetViewCenterXConstraint];
    [navigationBar addConstraint:bgTetViewCenterYConstraint];
    // END: Hinh background tet

    // Add buttons to navigation bar
    [navigationBar addSubview:backButton];
    [navigationBar addSubview:menuButton];
    [navigationBar addSubview:titleButton];
    [navigationBar addSubview:shareButton];
    [navigationBar addSubview:cartButton];

    // Add constraint to back button
    NSLayoutConstraint *backBtnLeftConstraint = [NSLayoutConstraint constraintWithItem:backButton attribute:NSLayoutAttributeLeft
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:navigationBar
                                                                             attribute:NSLayoutAttributeLeft
                                                                            multiplier:1.0 constant: 0];
    NSLayoutConstraint *backBtnCenterYConstraint = [NSLayoutConstraint constraintWithItem:backButton
                                                                                attribute:NSLayoutAttributeCenterY
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:navigationBar
                                                                                attribute:NSLayoutAttributeCenterY
                                                                               multiplier:1.0 constant:12];

    [navigationBar addConstraint:backBtnLeftConstraint];
    [navigationBar addConstraint:backBtnCenterYConstraint];

    // Add constraint to menu button
    NSLayoutConstraint *menuBtnLeftConstraint = [NSLayoutConstraint constraintWithItem:menuButton
                                                                             attribute:NSLayoutAttributeLeft
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:backButton
                                                                             attribute:NSLayoutAttributeRight
                                                                            multiplier:1.0 constant:0];
    NSLayoutConstraint *menuBtnCenterYConstraint = [NSLayoutConstraint constraintWithItem:menuButton
                                                                                attribute:NSLayoutAttributeCenterY
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:backButton
                                                                                attribute:NSLayoutAttributeCenterY
                                                                               multiplier:1.0 constant:0];
    [navigationBar addConstraint:menuBtnLeftConstraint];
    [navigationBar addConstraint:menuBtnCenterYConstraint];

    // Add constraint to titlt button
    NSLayoutConstraint *titleBtnCenterXConstraint = [NSLayoutConstraint constraintWithItem:titleButton
                                                                                 attribute:NSLayoutAttributeCenterX
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:navigationBar
                                                                                 attribute:NSLayoutAttributeCenterX
                                                                                multiplier:1.0 constant:0];
    NSLayoutConstraint *titleBtnCenterYConstraint = [NSLayoutConstraint constraintWithItem:titleButton
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:backButton
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                multiplier:1.0 constant:0];
    [navigationBar addConstraint:titleBtnCenterXConstraint];
    [navigationBar addConstraint:titleBtnCenterYConstraint];

    // Add constraint to cart button
    NSLayoutConstraint *cartBtnRightConstraint = [NSLayoutConstraint constraintWithItem:cartButton
                                                                              attribute:NSLayoutAttributeRight
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:navigationBar
                                                                              attribute:NSLayoutAttributeRight
                                                                             multiplier:1.0 constant:0];
    NSLayoutConstraint *cartBtnCenterYConstraint = [NSLayoutConstraint constraintWithItem:cartButton
                                                                                attribute:NSLayoutAttributeCenterY
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:backButton
                                                                                attribute:NSLayoutAttributeCenterY
                                                                               multiplier:1.0 constant:0];
    [navigationBar addConstraint:cartBtnRightConstraint];
    [navigationBar addConstraint:cartBtnCenterYConstraint];

    // Add constraint to search button
    NSLayoutConstraint *shareBtnRightConstraint = [NSLayoutConstraint constraintWithItem:shareButton
                                                                               attribute:NSLayoutAttributeRight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:cartButton
                                                                               attribute:NSLayoutAttributeLeft
                                                                              multiplier:1.0 constant:0];
    NSLayoutConstraint *shareBtnCenterYConstraint = [NSLayoutConstraint constraintWithItem:shareButton
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:backButton
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                multiplier:1.0 constant:0];
    [navigationBar addConstraint:shareBtnRightConstraint];
    [navigationBar addConstraint:shareBtnCenterYConstraint];
    shareButton.hidden = YES;
    
    // Resize views
    //[backButton sizeToFit];
    [menuButton sizeToFit];
    [titleButton sizeToFit];
    [cartButton sizeToFit];
    [shareButton sizeToFit];
    [navigationBar sizeToFit];
    
    txtStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [txtStatus setText:@"Không thể tải dữ liệu.\r\nVui lòng kiểm tra kết nối mạng và thử lại."];
    [txtStatus setTextAlignment:NSTextAlignmentCenter];
    txtStatus.numberOfLines = 5;
    txtStatus.lineBreakMode = NSLineBreakByWordWrapping;
    [txtStatus setTextColor: [UIColor redColor]];
    [self.viewController.view addSubview:txtStatus];
    
    NSLayoutConstraint *txtStatusCenterXConstraint = [NSLayoutConstraint constraintWithItem:txtStatus attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.viewController.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *txtStatusCenterYConstraint = [NSLayoutConstraint constraintWithItem:txtStatus attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.viewController.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
    [txtStatus setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.viewController.view addConstraint:txtStatusCenterXConstraint];
    [self.viewController.view addConstraint:txtStatusCenterYConstraint];
    [txtStatus sizeToFit];
    txtStatus.hidden = YES;
    // Retry button
    retryButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [retryButton setTitle:@"Thử lại" forState:UIControlStateNormal];
    [self.viewController.view addSubview:retryButton];
    
    NSLayoutConstraint *btnRetryTopConstraint = [NSLayoutConstraint constraintWithItem:retryButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:txtStatus attribute:NSLayoutAttributeBottom multiplier:1.0f constant:64.0f];
    NSLayoutConstraint *btnRetryCenterXConstraint = [NSLayoutConstraint constraintWithItem:retryButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:txtStatus attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    [retryButton setTranslatesAutoresizingMaskIntoConstraints:false];
    [self.viewController.view addConstraint:btnRetryTopConstraint];
    [self.viewController.view addConstraint:btnRetryCenterXConstraint];
    [retryButton sizeToFit];
    [retryButton addTarget:self action:@selector(webViewReloadPage:) forControlEvents:UIControlEventTouchUpInside];
    retryButton.hidden = YES;
    
    // Progress Bar
    progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    progressBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 8);
    [progressBar setTintColor: [UIColor grayColor]];
    [self.viewController.view setTranslatesAutoresizingMaskIntoConstraints:false];
    [self.viewController.view addSubview:progressBar];
    NSLayoutConstraint *constraintForProgressView = [NSLayoutConstraint constraintWithItem:progressBar
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:navigationBar
                                                                                 attribute:NSLayoutAttributeBottom
                                                                                multiplier:1.0f
                                                                                  constant:0.0f];
    [self.viewController.view addConstraint:constraintForProgressView];
    
    // Progress Ring
    progressRing = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    progressRing.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    [self.viewController.view addSubview:progressRing];
    NSLayoutConstraint *constraintCenterXForProgressRing = [NSLayoutConstraint constraintWithItem:progressRing attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.viewController.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *constraintCenterYForProgressRing = [NSLayoutConstraint constraintWithItem:progressRing attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.viewController.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
    [progressRing setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.viewController.view addConstraint:constraintCenterXForProgressRing];
    [self.viewController.view addConstraint:constraintCenterYForProgressRing];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    navigationBar.hidden = YES;
    
    // Save original webview delegate
    originWebViewDelegate = ((UIWebView*)self.webView).delegate;
    ((UIWebView*)self.webView).delegate = self;
}

bool isFirstLoad = true;
bool isGoingBack=false;
bool isReloadAfterError=false;
bool isTitleButtonClicked=false;
bool isCartButtonClicked=false;
NSInteger frameLevel=0;
NSURLRequest *rootFrameRequest;

- (void)hideBackButton{
    if (backButton.center.x > 0){
        [UIView animateWithDuration: 0.3
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             menuButton.center = CGPointMake(backButton.center.x, backButton.center.y);
                             backButton.center = CGPointMake(-backButton.center.x, backButton.center.y);
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
}
- (void)showBackButton{
    if (backButton.center.x < 0) {
        [UIView animateWithDuration: 0.3
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             backButton.center = CGPointMake(-backButton.center.x, backButton.center.y);
                             menuButton.center = CGPointMake(navigationBar.frame.origin.x + backButton.frame.size.width + menuButton.frame.size.width/2, backButton.center.y);
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
}

bool isInRootFrame = false;

// Implement UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    BOOL shouldLoad = YES;
    shouldLoad = [originWebViewDelegate webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    if (shouldLoad) {
        if (frameLevel == 0) {
            isGoingBack = isRetringAfterGoingBackError || (navigationType == UIWebViewNavigationTypeBackForward);
            
            if (isFirstLoad || isReloadAfterError || isTitleButtonClicked || isCartButtonClicked || navigationType==UIWebViewNavigationTypeBackForward || navigationType==UIWebViewNavigationTypeLinkClicked || navigationType ==UIWebViewNavigationTypeFormSubmitted || navigationType==UIWebViewNavigationTypeReload || navigationType==UIWebViewNavigationTypeFormResubmitted) {
                isInRootFrame = true;
                rootFrameRequest = request;// Save request for reloading if error occurr
                isTitleButtonClicked = false;
                isCartButtonClicked = false;
            } else {
                isInRootFrame = false;
            }
        }
    }
    return shouldLoad;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    //    NSLog(@"%@: webViewDidStartLoad", TAG);
    [originWebViewDelegate webViewDidStartLoad:webView];// Call original delegate
    frameLevel++;
    if (frameLevel==1 && isInRootFrame) {
        // Show or hide back button
        if ([webView canGoBack]) [self showBackButton];
        else [self hideBackButton];
        
        // Display indicator
        progressBar.progress = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES]; //0.02 is roughly 1/50, so it will update at 50 FPS
        // Play animation if request is not first request and not after error.
        if (!isFirstLoad && !isReloadAfterError) {
            [UIView animateWithDuration: 0.33f delay: 0.0f
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 webView.center = CGPointMake(webView.center.x + (isGoingBack?1:-1)*webView.frame.size.width, webView.center.y);
                             }
                             completion:^(BOOL finished){
                                 webView.alpha = 0.0f;
                                 NSLog(@"%@: TanND An webview", TAG);
                             }];
        }
        
        // Mark that later requests is not first request yet
        isFirstLoad = false;
        
        isReloadAfterError = false;
    }
}

-(void)timerCallback {
    if (progressBar.progress == 0) {
        progressBar.hidden = false;
        [progressRing startAnimating];
    }
    if (frameLevel>0&&isInRootFrame) {
        if (progressBar.progress<0.9) {
            progressBar.progress+=0.01;
        }
    }
    else {
        progressBar.progress += 0.05;
    }
    if (progressBar.progress >= 1) {
        progressBar.hidden = true;
        [progressRing stopAnimating];
        [timer invalidate];
    }
}

BOOL isRetringAfterGoingBackError;

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    //    NSLog(@"%@: didFailLoadWithError %@", TAG, webView.isLoading?@"Still loading":nil);// Write log
    [originWebViewDelegate webViewDidFinishLoad:webView]; // Call original delegate
    if (frameLevel==1&&isInRootFrame&&error.code!=NSURLErrorCancelled) {
        // Show retry button
        retryButton.hidden = NO;
        txtStatus.hidden = NO;
        if (isGoingBack) {
            isRetringAfterGoingBackError = true;
        } else {
            isRetringAfterGoingBackError = false;
        }
        isGoingBack = false;
    };
    frameLevel--;
}

- (void)webViewReloadPage:(UIButton*)sender{
    // Hide retry button
    retryButton.hidden = YES;
    txtStatus.hidden = YES;
    // Mark that webview is loading after error
    isReloadAfterError = true;
    // Reload last request
    if (isRetringAfterGoingBackError) {
        NSString *targetLocation = rootFrameRequest.URL.absoluteString;
        [(UIWebView*)self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.setTimeout( \"window.location.replace( '%@' )\", 300 );", targetLocation]];
    } else {
        [(UIWebView*)self.webView loadRequest:rootFrameRequest];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    //    NSLog(@"%@: webViewDidFinishLoad: %li, %i", TAG,(long)frameLevel,isInRootFrame);// Write log
    [originWebViewDelegate webViewDidFinishLoad:webView];// Call original delegate
    if (frameLevel==1 && isInRootFrame) {
        NSInteger centerX = webView.superview.center.x;
        NSInteger centerY = webView.superview.center.y;
        webView.center = CGPointMake(centerX, centerY);
        webView.alpha = 0.0f;
        [UIView animateWithDuration: 0.33f delay: 0.33f
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             webView.alpha = 1.0f;
                             webView.hidden = NO;
                         }
                         completion:^(BOOL finished){
                             NSLog(@"%@: TanND Hien webview o (x,y)=(%li,%li)", TAG,(long)centerX,(long)centerY);
                             webView.alpha = 1.0f;
                             webView.hidden = NO;
                             webView.center = CGPointMake(centerX, centerY);
                         }];
        retryButton.hidden = YES;
        txtStatus.hidden = YES;
        isGoingBack = false;
        isRetringAfterGoingBackError = NO;
    }
    frameLevel--;
}
- (void)showView:(CDVInvokedUrlCommand*)command{
    NSString *viewName = [command.arguments objectAtIndex:0];
    [self _showView:viewName];
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}
- (void)hideView:(CDVInvokedUrlCommand*)command{
    NSString *viewName = [command.arguments objectAtIndex:0];
    [self _hideView:viewName];
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)_showView:(NSString*)viewName{
    NSLog(@"Show view: %@", viewName);
    if ([viewName isEqualToString:@"NAVIGATIONBAR"]) {
        if (navigationBar.center.y<0){
            [UIView animateWithDuration: 0.3f
                                  delay: 0.0f
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 navigationBar.center = CGPointMake(navigationBar.center.x, -navigationBar.center.y);
                                 navigationBar.alpha = 1.0f;
                             }
                             completion:^(BOOL finished){
                                 
                             }];
        }
    } else if ([viewName isEqualToString:@"BACK"]) {
        [self showBackButton];
    } else if ([viewName isEqualToString:@"MENU"]) {
        menuButton.hidden = false;
    } else if ([viewName isEqualToString:@"TITLE"]) {
        titleButton.hidden = false;
    } else if ([viewName isEqualToString:@"SHARE"]) {
        shareButton.hidden = false;
    } else if ([viewName isEqualToString:@"CART"]) {
        cartButton.hidden = false;
    }
}
- (void)_hideView:(NSString*)viewName{
    NSLog(@"Hide view: %@", viewName);
    if ([viewName isEqualToString:@"NAVIGATIONBAR"]) {
        if (navigationBar.center.y>0){
            [UIView animateWithDuration: 0.3f
                                  delay: 0.0f
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 navigationBar.center = CGPointMake(navigationBar.center.x, -navigationBar.center.y);
                                 navigationBar.alpha = 0.0f;
                             }
                             completion:^(BOOL finished){
                             }];
        }
    } else if ([viewName isEqualToString:@"BACK"]) {
        [self hideBackButton];
    } else if ([viewName isEqualToString:@"MENU"]) {
        menuButton.hidden = true;
    } else if ([viewName isEqualToString:@"TITLE"]) {
        titleButton.hidden = true;
    } else if ([viewName isEqualToString:@"SHARE"]) {
        shareButton.hidden = true;
    } else if ([viewName isEqualToString:@"CART"]) {
        cartButton.hidden = true;
    }
}
- (void)_setText:(NSString*)text forView:(NSString*)viewName{
    NSLog(@"Set text: %@ for %@",text, viewName);
    if ([viewName isEqualToString:@"BACK"]) {
        [backButton setTitle:text forState:UIControlStateNormal];
    } else if ([viewName isEqualToString:@"MENU"]) {
        [menuButton setTitle:text forState:UIControlStateNormal];
    } else if ([viewName isEqualToString:@"TITLE"]) {
        [titleButton setTitle:text forState:UIControlStateNormal];
        if ([text length] == 0){
            [titleButton setImage:[UIImage imageNamed:@"ic_logo_nk"] forState:UIControlStateNormal];
            [titleButton setTitle:nil forState:UIControlStateNormal];
        } else {
            [titleButton setImage:nil forState:UIControlStateNormal];
            [titleButton setTitle:text forState:UIControlStateNormal];
        }
    } else if ([viewName isEqualToString:@"SHARE"]) {
        [shareButton setTitle:text forState:UIControlStateNormal];
    } else if ([viewName isEqualToString:@"CART"]) {
        [cartButton setTitle:text forState:UIControlStateNormal];
    }
}

- (void)_clearCache {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}
- (void)_clearHistory {
    
}
- (BOOL)_canGoBack {
    return [(UIWebView*)self.webView canGoBack];
}

@end

